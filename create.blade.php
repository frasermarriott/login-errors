@extends('layouts/master')

@section('title', 'Create an account')

@section('content')

	<h2>Create an account</h2>

	@if(count($errors))
		<div class="alert alert-danger">
			<ul>
				@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div> 
	@endif

	{!! Form::open(array('route' => 'users.store')) !!}

		<div class="form-group">
			{!! Form::label('name', 'Username') !!}
			{!! Form::text('name', null, array('class' => 'form-control', 'required', 'autofocus')) !!}
		</div>

		<div class="form-group">
			{!! Form::label('email') !!}
			{!! Form::email('email', null, array('class' => 'form-control', 'required')) !!}
		</div>

		<div class="form-group">
			{!! Form::label('password') !!}
			{!! Form::password('password', array('class' => 'form-control', 'required')) !!}
		</div>

		{!! Form::token() !!}
		{!! Form::submit(null, array('class' => 'btn btn-default')) !!}

	{!! Form::close() !!}


@endsection