<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
// use Auth;
// use Illuminate\Support\MessageBag;


class AuthController extends Controller
{
	// Login
    public function login()
    {
    	return view('auth.login');
    }

    // Handle the login process, if successful, redirect to User Profile, if not, return back.
    public function handleLogin(Request $request)
    {
    	$this->validate($request, User::$login_validation_rules);
    	$data = $request->only('email', 'password');
    	if(\Auth::attempt($data)){
    		return redirect()->intended('profile');
    	}

    	return back()->withInput();
        // return redirect()->back()->withInput();
    }

    public function logout()
    {
    	\Auth::logout();
    	return redirect()->route('login');
    }

}
